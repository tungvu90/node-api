/*
 Navicat Premium Data Transfer

 Source Server         : DB
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : node_api

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 05/12/2022 14:20:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `registered` datetime NULL DEFAULT NULL,
  `last_login` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'tungvu90', '$2y$10$NYeCCEOQiSNhO7ySjy1V5uJSvYogUq3KLb3Bnm8H5Mg1HU0rHUbz2', NULL, NULL);
INSERT INTO `users` VALUES (2, 'tungvu80', '$2a$10$mKV6k6.9WvDyF6uWciJXL.ziuKB0vK5OarIPNTIu0l.saWsRFjKvC', '2022-12-05 11:59:57', '2022-12-05 13:56:07');
INSERT INTO `users` VALUES (3, 'tungvu70', '$2a$10$SjKkf8moMSFNUgrNP3DZzuqr/boGMgRxmAbFSDxMMnBGRUGCXfRt.', '2022-12-05 13:58:02', NULL);

SET FOREIGN_KEY_CHECKS = 1;
