import mysql from 'mysql';

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'node_api',
    password: ''
});
connection.connect();
export default connection;