import express from 'express';

const app = express();
import cors from 'cors';
// set up port
const PORT = process.env.PORT || 3000;
app.use(express.json());
app.use(cors());
// add routes
import router from './routes/router.js';

app.use('/api', router);
// run server
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));